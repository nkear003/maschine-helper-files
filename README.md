*Note: This README is incomplete on purpose. I uploaded this to help people, but
don't want to commit so much time to making it verbose, unless I see enough 
interest and/or questions in forums* 

# About

I made a few scripts to help with using NI Maschine. Some examples include 
making new project folders and copying projects/groups, especially where you 
used "Save Project With Samples," or "Save Group with Samples" to keep your 
projects intact,  but also want to have them available in your browser without 
creating duplicate samples. 

I didn't go into detail on how the files work, use variables, or even finish
filling out the README, just because I don't have a lot of free time on my 
hands. If I answer questions here, I will try  to fill them in on the Gitlab 
repo, and maybe could even turn it into a CLI, or at least make it a bit more 
user friendly.

I'm on Mac. The scripts and instructions may need some adapting for 
Windows/other OSs.

# Usage

1. [Download](https://gitlab.com/nkear003/maschine-helper-files/-/archive/master/maschine-helper-files-master.zip) the files to your computer
2. Change the locations where files will save to one your maschine and customise 
directory structure to your preference
3. Open Terminal
4. Change directory where you downloaded scripts to
5. Use command `chmod +x <filename>` to make the files executeable
6. Click either individual files from finder or the file that starts with a '#'
to run them all

# Breakdown of Files

*Note: I use the date format YYMMDD for timestamps*

## copyMaschineFilesFromProjectsAndLiveSets

## copyMaschineGroupFiles

## copyMaschineProjectFiles


## newProjectFolder

- Makes a directory in my music projects folder with time stamp as name
- Create a empty file with time stamp as name (so you can click it to set the 
name of the project file and add notes)
- Opens the folder in finder